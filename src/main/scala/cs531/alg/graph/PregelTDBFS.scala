package cs531.alg.graph

import org.apache.spark.graphx._

/**
// Time dependent BFS (Breadth First Search)
// Vertex Data: (Predecessor:VertexId, Distance:Double, Duration:Double, Locked:Boolean) => (VertexId, Double, Double, Boolean)
// Edge Data: Distance:Double
// Message: (Predecessor:VertexId, Distance:Double, Duration:Double) => (VertexId, Double, Double)
 */
object PregelTDBFS 
{
  def vprog(vertexId:VertexId, value:(VertexId, Double, Double, Boolean), msg:(VertexId, Double, Double)): (VertexId, Double, Double, Boolean) =
  {
    // if vertex is not locked/finalized
    if(!value._4)
    {
      if(value._2 > msg._2)
        return (msg._1, msg._2, msg._3, value._4)
      else
        return value      
    }
    else
      return value

  }
  
  def sendMsg(triplet:EdgeTriplet[(VertexId, Double, Double, Boolean), Double]): Iterator[(VertexId, (VertexId, Double, Double))] = 
  {
    // if neighbor is not locked and duration-left >= edge-weight 
    if(triplet.dstAttr._4 == false && triplet.srcAttr._3 >= triplet.attr)
    {
      // if distance + edge-weight < neighbor.distance then relax that edge
      if(triplet.srcAttr._2 + triplet.attr < triplet.dstAttr._2)
      {
        // send to neighbor (this.id, distance + edge-weight, duration - edge-weight)
        return Iterator((triplet.dstId, (triplet.srcId, triplet.srcAttr._2 + triplet.attr, triplet.srcAttr._3 - triplet.attr)))
      }
      else
      {
        return Iterator.empty
      }
    }
    else // we don't have enough time to spend on any outgoing edge, algorithm terminates in this snapshot
    {
      return Iterator.empty
    }
  }
  
  // reduce any message by picking the one with smaller distance value and forwarding it.
  def mergeMsg(msg1:(VertexId, Double, Double), msg2:(VertexId, Double, Double)): (VertexId, Double, Double) = 
  {
    if(msg2._2 < msg1._2)
    {
      return msg2
    }
    else
      return msg1
  }
}