package cs531.alg.graph

import org.apache.spark.graphx._

object PregelSSSP_Predecessor
{
  def vprog(vertexId:VertexId, value:(VertexId, Double), message:(VertexId, Double)): (VertexId, Double) =
  {
    if(value._2 > message._2)
      return value
    else
      return message
  }
  
  def sendMsg(triplet:EdgeTriplet[(VertexId, Double), Double]): Iterator[(VertexId, (VertexId, Double))] = 
  {
    if(triplet.srcAttr._2 + triplet.attr < triplet.dstAttr._2)
      return Iterator((triplet.dstId, (triplet.srcId, triplet.srcAttr._2 + triplet.attr)))
    else
      return Iterator.empty
  }
  
  def mergeMsg(msg1:(VertexId, Double), msg2:(VertexId, Double)): (VertexId, Double) = 
  {
    if(msg1._2 < msg2._2)
      return msg1
    else
      return msg2
  }
}