package cs531.alg.graph

import cs531.graph.SparseGraph
import cs531.graph.Vertex
import scala.collection.mutable.PriorityQueue
import cs531.graph.VertexOrdering
import cs531.graph.CustomEdge

object SSSP 
{
  def bellmanFord(graph:SparseGraph, s:Vertex): Boolean = 
  {
    graph.initializeSingleSource(s)
    var i = 0 
    
    for(i <- 0 until graph.vertexCount - 1)
    {
      for(j <- 0 until graph.edgeCount)
      {
        var e:CustomEdge = graph.edges(j)
        relax(graph, e)
      }
    }
    
    for(j <- 0 until graph.edgeCount)
    {
      var e:CustomEdge = graph.edges(j)
      
      var u:Vertex = graph.vertices(e.srcId)
      var v:Vertex = graph.vertices(e.dstId)
      var w:Double = e.weight
      
      if(v.satellite.d > u.satellite.d + w)
        return false;
    }
    
    return true
  }
  
  def dijkstra(graph:SparseGraph, s:Vertex): Unit =
  {
    graph.initializeSingleSource(s)
    var Q:PriorityQueue[Vertex] = new PriorityQueue[Vertex]()(VertexOrdering)
    Q.enqueue(s)
    // no min head implementation??
    // graph.vertices.foreach { v => Q.enqueue(v) }    
    
    while(!Q.isEmpty)
    {
      var u:Vertex = Q.dequeue()
      
      var w:Double = 0.0
      var vid:Int = 0
      for((vid, w) <- graph.adjList(u.id))
      {
        var v:Vertex = graph.vertices(vid)
        var vd = v.satellite.d
        relax(u, v, w)
        if(vd > v.satellite.d)
        {
          Q.enqueue(v)
        }
      }
    }
  }
  
  def relax(graph:SparseGraph, e:CustomEdge): Unit =
  {
    var u:Vertex = graph.vertices(e.srcId)
    var v:Vertex = graph.vertices(e.dstId)
    var w:Double = e.weight
    
    relax(u, v, w) 
  }
  
  def relax(u:Vertex, v:Vertex, w:Double): Unit = 
  {
    if(v.satellite.d > u.satellite.d + w)
    {
      v.satellite.d = u.satellite.d + w
      v.satellite.p = u.id
    }
  }
}