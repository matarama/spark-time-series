package cs531.alg.graph

import org.apache.spark.graphx._

object PregelAPSP 
{
  def vprog(vertexId:VertexId, value:Double, message:Double): Double =
  {
    return math.min(value, message)
  }
  
  def sendMsg(triplet:EdgeTriplet[Double, Double]): Iterator[(VertexId, Double)] = 
  {
    if(triplet.srcAttr + triplet.attr < triplet.dstAttr)
      return Iterator((triplet.dstId, triplet.srcAttr + triplet.attr))
    else
      return Iterator.empty
  }
  
  def mergeMsg(msg1:Double, msg2:Double): Double = 
  {
    return math.min(msg1, msg2)
  }    
}