package cs531.alg.graph

import cs531.graph.TVSparseGraph
import cs531.graph.Vertex
import cs531.graph.CustomSnapshot
import scala.util.control.BreakControl
import scala.collection.mutable.PriorityQueue
import cs531.graph.SparseGraph
import scala.collection.mutable.Map
import scala.collection.mutable.HashMap

object TDSP 
{
  // Returns a chain of (distance array per vertex, predecessor-snapshot index array per vertex)
  def dijkstra(T:TVSparseGraph, sId:Int, time:Double): (Array[Double], Array[(Int, Int)]) = 
  {
    var n:Int = T.vertexCount
    var D:Array[Double] = new Array[Double](n) // Distance from source vertex
    var C:Array[Double] = new Array[Double](n) // Time used in the current snapshot on this vertex
    var P:Array[(Int, Int)] = new Array[(Int, Int)](n) // Predecessor vertex id, snapshot index
    for(i:Int <- 0 until n)
    {
      D(i) = Double.PositiveInfinity
      C(i) = 0.0
      P(i) = (-1, -1)
    }
    
    // snapshot index that overlaps with the given time value
    var ssIndex:Int = T.findSnapshotIndex(time)
    
    // what is left in current snapshot
    var duration = T.snapshots(ssIndex).clock + T.snapshots(ssIndex).duration - time 
    
    var timeElapsed:Double = 0.0
    P(sId) = (sId, ssIndex)
    D(sId) = 0.0
    C(sId) = 0.0
    
    // Min priority queue keyed on second element
    var activeVertices:PriorityQueue[(Int, Double)] = 
      new PriorityQueue[(Int, Double)]()(Ordering[(Double)].reverse.on(x => (x._2)))
    activeVertices.enqueue((sId, D(sId)))
    
    for(i:Int <- ssIndex until T.snapshots.length)
    {
      var currSnapshot:CustomSnapshot = T.snapshots(i)
      var g:SparseGraph = currSnapshot.graph
      var nextVertices:HashMap[Int, Int] = new HashMap()
      
      while(!activeVertices.isEmpty)
      {
        var uId:Int = activeVertices.dequeue()._1 // source vertex id
        var neighborsDiscovered:Boolean = true
        
        g.adjList(uId).foreach(e => 
          {
            var vId:Int = e._1
            var w:Double = e._2
            if(w <= duration - C(uId))
            {
              if(D(vId) > (math.max(timeElapsed, D(uId)) + w))
              {
                D(vId) = math.max(timeElapsed, D(uId)) + w
                C(vId) = w
                P(vId) = (uId, i)
                activeVertices.enqueue((vId, D(vId)))
              }
            }
            // if at least one of the neighboring vertices are not discovered, 
            // try to visit them once more in the next snapshot
            else if(D(vId) == Double.PositiveInfinity) 
            {
              nextVertices.put(uId, uId)
            }
          }
        )
      }
      
      timeElapsed = timeElapsed + duration
      nextVertices.foreach(
          (pair) => 
          {
            activeVertices.enqueue((pair._1, timeElapsed))
            
            // end of this vertex's turn in the current snapshot, 
            // prepare for the next one since it will be active there
            C(pair._1) = 0.0
          }
        )
      
      if(i + 1 < T.snapshots.length)
        duration = T.snapshots(i + 1).duration
    }
    
    return (D, P)
  }
  
  /**
   * @P: Predecessor vertex id, snapshot index array returned from time depended dijkstra
   * @d: Destination vertex id
   */
  def reconstructPath(P:Array[(Int, Int)], d:Int): String =
  {
    var vid:Int = d
    var path:String = vid  + ", " + P(vid)._2
    
    while(P(vid)._1 != vid)
    {
      vid = P(vid)._1
      path = path + " => " + vid + ", " + P(vid)._2
    }
    
    return path
  }
}