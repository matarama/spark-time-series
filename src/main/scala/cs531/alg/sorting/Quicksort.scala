
package cs531.alg.sorting

import cs531.sparse.Triplet

object Quicksort 
{  
  def quicksort(triplets:Array[Triplet], alg:QuicksortPartition): Unit = 
  {
    quicksort(triplets, 0, triplets.length - 1, alg)
  }
  
  def quicksortTail(triplets:Array[Triplet], alg:QuicksortPartition): Unit =
  {
    quicksortTail(triplets, 0, triplets.length - 1, alg)
  }
  
  // ------------------------------------------------------------------------------------
  
  private def quicksort(triplets:Array[Triplet], p:Int, r:Int, alg:QuicksortPartition): Unit = 
  {
    if(p < r)
    {
      var q:Int = alg.partition(triplets, p, r)
      quicksort(triplets, p, q - 1, alg)
      quicksort(triplets, q + 1, r, alg)
    }
  }
  
  private def quicksortTail(triplets:Array[Triplet], head:Int, tail:Int, alg:QuicksortPartition): Unit = 
  {
    var p:Int = head
    var r:Int = tail
    while(p < r)
    {
      var q:Int = alg.partition(triplets, p, r)
      quicksortTail(triplets, p, q - 1, alg)
      p = q + 1
    }
  }
}