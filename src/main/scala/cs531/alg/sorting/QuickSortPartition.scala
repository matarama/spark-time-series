package cs531.alg.sorting

import cs531.sparse.Triplet

trait QuicksortPartition 
{
  def partition(triplets:Array[Triplet], p:Int, r:Int): Int   
}