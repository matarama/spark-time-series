package cs531.alg.sorting

import scala.util.Random
import cs531.sparse.Triplet

object LomutoPartition extends QuicksortPartition
{
  def partition(triplets:Array[Triplet], p:Int, r:Int): Int = 
  {
    // Choose a random pivot
    var q:Int = Random.nextInt(r - p) + p
    
    var pivot:Triplet = triplets(q)
    triplets(q) = triplets(r)
    triplets(r) = pivot
    
    var j:Int = p - 1
    var i:Int = j
    while(i < r)
    {
      i += 1
      if(pivot.compareTo(triplets(i)) > 0)
      {
        j += 1
        var temp:Triplet = triplets(i)
        triplets(i) = triplets(j)
        triplets(j) = temp
      }
    }
    
    var temp:Triplet = triplets(i)
    triplets(i) = triplets(j)
    triplets(j) = temp
    
    return j + 1
  }
}