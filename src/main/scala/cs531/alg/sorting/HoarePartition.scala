package cs531.alg.sorting

import scala.util.Random
import cs531.sparse.Triplet

object HoarePartition extends QuicksortPartition
{
  def partition(triplets:Array[Triplet], p:Int, r:Int): Int = 
  {
    // Choose a random pivot
    var q = Random.nextInt(r - p) + p
    
    var pivot:Triplet = triplets(q)
    triplets(q) = triplets(r)
    triplets(r) = pivot
    
    var i = p
    var j = r
    while(true)
    {
      while(pivot.compareTo(triplets(j)) < 0)
        j -= 1
        
      while(pivot.compareTo(triplets(i)) > 0)
        i += 1
      
      if(i < j)
      {
        var temp:Triplet = triplets(i)
        triplets(i) = triplets(j)
        triplets(j) = temp
      }
      else
      {
        return j
      }
    }
    
    return -1
  }
}