package cs531.sparse

class Triplet(RowId:Int = -1, ColId:Int = -1, Value:Double = 1) 
{
  var rowId:Int = RowId
  var colId:Int = ColId
  var value:Double = Value
  
  def parse(line:String): Triplet = 
  {
    var temp:Array[String] = line.split(" ");
    rowId = temp(0).toInt - 1
    colId = temp(1).toInt - 1
    if(temp.length > 2)
      value = temp(2).toDouble
    
    return this
  }
  
  
  override def toString(): String = rowId + " " + colId + " " + value
  
  def compareTo(other:Triplet): Int = 
  { 
		if(this.rowId < other.rowId)
			return -1;
		else if(this.rowId > other.rowId)
			return 1;
		else
		{
			if(this.colId < other.colId)
				return -1;
			else if(this.colId > other.colId)
				return 1;
			else
				return 0;
		}
  }
}