
package cs531.sparse

import scala.io.Source
import cs531.alg.sorting.Quicksort
import cs531.alg.sorting.HoarePartition

class COO(RowCount:Int = 0, ColCount:Int = 0, NNZ:Int = 0)
{
  var rowCount:Int = RowCount
  var colCount:Int = ColCount
  var nnz:Int = NNZ
  var triplets:Array[Triplet] = new Array[Triplet](NNZ)
  
  def parseMMF(mmfPath:String): COO = 
  {
    var lines = Source.fromFile(mmfPath).getLines()
    var desc = lines.next()
    
    var isSymmetric = desc.contains("symmetric")
    
    var line = lines.next()
    while(line.charAt(0) == '%')
      line = lines.next()
      
    var dimStr:Array[String] = line.split(" ")
    
    rowCount = Integer.parseInt(dimStr(0))
    colCount = Integer.parseInt(dimStr(1))
    nnz = Integer.parseInt(dimStr(2))        
    
    var tList:List[Triplet] = Nil
    var i = 0
    for(i <- 0 until nnz)
    {
      line = lines.next()
      var t = new Triplet().parse(line)
      tList = t :: tList
      
      if(isSymmetric && t.colId != t.rowId)
        tList = new Triplet(t.colId, t.rowId, t.value) :: tList
    }
    
    triplets = tList.toArray
    nnz = triplets.length
    
    Quicksort.quicksortTail(triplets, HoarePartition);
    
    return this
  }
}
