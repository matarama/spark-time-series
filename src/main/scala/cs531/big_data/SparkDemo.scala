package cs531.big_data

import org.apache.spark._
import org.apache.spark.graphx._
import cs531.graph.TVSparkGraph
import cs531.graph.TVSparkGraph
import cs531.alg.graph.PregelTDBFS
import org.apache.spark.graphx.Graph.graphToGraphOps
import org.apache.spark.broadcast.Broadcast

object SparkDemo 
{
  def main(args : Array[String])
  {
    val conf = new SparkConf()
      .setAppName("TDSP (time dependent shortest paths)")
      .setMaster("local[2]")

    val sc: SparkContext = new SparkContext(conf)
    
    
    var T:TVSparkGraph = new TVSparkGraph().readSnapshots(sc, 
        "input/time-varying0.mtx", 
        "input/time-varying1.mtx", 
        "input/time-varying2.mtx")
    
    // Run TDSP starting from vertex 7 (6L)
    println("Running Hybrid-TDSP Algorithm...")
    var graph = T.snapshots(0).graph.mapVertices((id, value) => if(id == 6L) (id, 0.0, value._3, value._4) else value)
    var ssIndex = 1
    var sssp = graph.pregel((-1L, Double.PositiveInfinity, -1.0))(
          PregelTDBFS.vprog, 
          PregelTDBFS.sendMsg, 
          PregelTDBFS.mergeMsg)
    
    while(ssIndex < T.snapshots.length)
    {
      // find finalized / locked vertices (ones that have their distance calculated)
      val lockedVertices:Array[(VertexId, (VertexId, Double, Double, Boolean))] = 
        sssp.vertices.mapValues(prop => if(prop._1 != -1) (prop._1, prop._2, prop._3, true) else prop).collect()
        
      var vMap:Map[VertexId, (VertexId, Double, Double, Boolean)] = Map()
      lockedVertices.foreach(value => if(value._2._4) vMap += (value._1 -> value._2))
      println("Locked Vertices")
      println(lockedVertices.mkString("\n"));
      println()
    
      // update vertices' properties in the next snapshot (that is discovered in current snapshot)
      val timeElapsed:Broadcast[Double] = sc.broadcast(T.snapshots(ssIndex).clock)
      val snapshot = T.snapshots(ssIndex)
      graph = snapshot.graph.mapVertices((id, prop) 
          => if(vMap.contains(id)) (vMap(id)._1, timeElapsed.value, prop._3, vMap(id)._4) else prop)
      
      println(graph.vertices.collect().mkString("\n"))
      println(graph.edges.collect().mkString("\n"))
      println()
      println()
      
      // snapshot.graph.vertices.mapValues((id, prop) => if(vMap.contains(id)) vMap(id) else prop )
    
      sssp = graph.pregel((-1L, Double.PositiveInfinity, -1.0))(
          PregelTDBFS.vprog, 
          PregelTDBFS.sendMsg, 
          PregelTDBFS.mergeMsg)
      
      ssIndex += 1
      // remove edges leading to already discovered vertices
      // val snapshot = T.snapshots(ssIndex).graph.subgraph(edge => !vMapBroadcasted.value.contains(edge.dstId), (id, prop) => true)      
    }
    
    println(sssp.vertices.collect().mkString("\n"))
    println()
    
    /*    
    // Run SSSP_Predecessor for each graph starting from vertex 7 (6L)
    println("Running PregelSSSP_Predecessor...")
    for(i:Int <- 0 until T.snapshots.length)
    {
      val graph = T.snapshots(i).graph
      val initialGraph = graph.mapVertices((id, value) => if(id == 6L) (id, (0.0)) else (-1L, Double.PositiveInfinity))
      val sssp = initialGraph.pregel((-1L, Double.PositiveInfinity))(
          PregelSSSP_Predecessor.vprog, 
          PregelSSSP_Predecessor.sendMsg, 
          PregelSSSP_Predecessor.mergeMsg)
      
      println(sssp.vertices.collect().mkString("\n"))
      println()
    }
    */
    
    println(T)
  }
  /*
   * type mismatch; found : Iterator[(org.apache.spark.graphx.VertexId, 
 (org.apache.spark.graphx.VertexId, Double))] (which expands to) 
 
 found: Iterator[(Long, (Long, Double))] 
 required: Iterator[(Long, Double)]
   */
/*
  def main(args : Array[String]) 
  {
    val conf = new SparkConf()
      .setAppName("The swankiest Spark app ever")
      .setMaster("local[2]")

    // Assume the SparkContext has already been constructed
    val sc: SparkContext = new SparkContext(conf)
    
    var T:TVSparkGraph = new TVSparkGraph().readSnapshots(sc, 
        "input/time-varying0.mtx", 
        "input/time-varying1.mtx", 
        "input/time-varying2.mtx")
    
    // Run SSSP for each graph starting from vertex 7 (6L)
    for(i:Int <- 0 until T.snapshots.length)
    {
      val graph = T.snapshots(i).graph
      val initialGraph = graph.mapVertices((id, _) => if(id == 6L) 0.0 else Double.PositiveInfinity)
      val sssp = initialGraph.pregel(Double.PositiveInfinity)(
          PregelSSSP.vprog, 
          PregelSSSP.sendMsg, 
          PregelSSSP.mergeMsg)
      
      println(sssp.vertices.collect().mkString("\n"))
      println()
    }
    
    println(T)
  }
  */
}
