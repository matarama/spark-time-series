
package cs531.sequential

import cs531.graph.TVSparseGraph
import cs531.alg.graph.TDSP

object SequentialDemo 
{
  def main(args: Array[String])
  {
    // Read and print time varying graph
    var T:TVSparseGraph = new TVSparseGraph().readSnapshots(
        "input/time-varying0.mtx", 
        "input/time-varying1.mtx", 
        "input/time-varying2.mtx")
    println(T)
    
    // Query time dependent shortest paths for 
    // (vertex-id, starting-time) pairs: (7, 0), (5, 4), and (5, 3)
    var sources:Array[Int] = Array(6, 4, 4)
    var startingTimes:Array[Double] = Array(0.0, 4.0, 3.0)
    
    for(i:Int <- 0 until sources.length)
    {
      println("TDSP(T, " + sources(i) + ", " + startingTimes(i) + ")")
      var result = TDSP.dijkstra(T, sources(i), startingTimes(i))
      var D:Array[Double] = result._1 
      var P:Array[(Int, Int)] = result._2
      
      println("Distance Array:")
      for(i:Int <- 0 until D.length)
        print(D(i) + " ")
      println()
        
      println("Predecessor Array:")
      P.foreach(p => print(p._1 + " "))
      println()
      P.foreach(p => print(p._2 + " "))
      println()
      println()
    }
  }
}