package cs531.graph

import cs531.sparse.COO
import org.apache.spark.SparkContext
import org.apache.spark.graphx._
import cs531.sparse.Triplet
import org.apache.spark.rdd.RDD

class TVSparkGraph 
{
  var snapshots:Array[SparkSnapshot] = null

  /**
   * Returned snapshot graphs have the following properties per,
   * Vertex => (Vertex-id, (Predecessor-id, distance, duration)) as its datatype representation (VertexId, (VertexId, Double, Double))
   * Edge => (Src-vertex-id, Dst-vertex-id, edge-weight) as its datatype representation (VertexId, VertexId, Double)
   */
  // TODO snapshot clock and duration should be parsed from file
  def readSnapshots(sc:SparkContext, mmfPaths:String*): TVSparkGraph =
  {    
    var clock:Double = 0.0
    var duration:Double = 5.0
    
    snapshots = new Array[SparkSnapshot](mmfPaths.length)
    
    for(ssIndex:Int <- 0 until snapshots.length)
    {
      val coo:COO = new COO().parseMMF(mmfPaths(ssIndex))
      snapshots(ssIndex) = new SparkSnapshot(clock, duration).extractFromCOO(coo, sc)
      
      clock += duration
    }
    
    return this
  }
  
  /**
   * Returned graph has:
   * Vertex => (Vertex-id, (Predecessor-id, distance)) as its datatype representation (VertexId, (VertexId, Double))
   * Edge => (Src-vertex-id, Dst-vertex-id, edge-weight) as its datatype representation (VertexId, VertexId, Double)
   * 
   * So returned graph has 
   * - (Predecessor-id, distance) property for each vertex
   * - (edge-weight) property for each edge
   * Which becomes Graph[(VertexId, Double), Double]
   * 
   * Additional return value in (Long, Long, Double, Double) stands for (start-vertex-id, end-vertex-id, clock, duration).
   * This is a range for vertices in each snapshot to check if they will be active at any time.
   */
  // TODO snapshot clock and duration should be parsed from file
  /*
  def readSnapshots(sc:SparkContext, mmfPaths:String*): (Graph[(VertexId, Double), Double], Array[(Long, Long, Double, Double)]) =
  {    
    var clock:Double = 0.0
    var duration:Double = 5.0
    
    // Generate a graph from the first snapshot
    var coo:COO = new COO().parseMMF(mmfPaths(0))
    snapshotVertexCount = coo.rowCount
    snapshotEdgeCount = coo.nnz
    
    // total vertex count becomes (vertex count per snapshot) * (number of snapshots) 
    val totalVertexCount = snapshotVertexCount * mmfPaths.length
    
    // total edge count becomes 
    // (edge count per snapshot) * (number of snapshots) +
    // edges between snapshots = (vertex count per snapshot) * (number of snapshots - 1)
    val totalEdgeCount = snapshotEdgeCount * mmfPaths.length + snapshotVertexCount * (mmfPaths.length - 1)
    
    // Merge all snapshots into one global graph (which will be distributed by spark)
    var vertices:Array[(VertexId, (VertexId, Double))] = new Array[(VertexId, (VertexId, Double))](totalVertexCount)
    var edges:Array[Edge[Double]] = new Array[Edge[Double]](totalEdgeCount)
    
    var vi:Int = 0 // vertex index of global graph
    var ei:Int = 0 // edge index of global graph
    
    // Add snapshot
    for(i:Int <- 0 until snapshotVertexCount)
    {   
      vertices(vi) = (i.toLong, (-1L, Double.PositiveInfinity))
      vi += 1
    }
    
    for(i:Int <- 0 until snapshotEdgeCount)
    {
      val t:Triplet = coo.triplets(i) 
      edges(ei) = Edge(t.rowId.toLong, t.colId.toLong, t.value)
      ei += 1
    }
    
    // add succeeding snapshots
    for(i:Int <- 1 until mmfPaths.length)
    {
      coo = new COO().parseMMF(mmfPaths(i))
      
      // add vertices
      for(j:Int <- 0 until snapshotVertexCount.toInt)
      {
        vertices(vi) = (j.toLong + i * snapshotVertexCount, (-1, Double.PositiveInfinity))
        vi += 1
      }
      
      // add edges in snapshot
      for(j:Int <- 0 until snapshotEdgeCount.toInt)
      {
        val t:Triplet = coo.triplets(i)
        edges(ei) = Edge(t.rowId + i * snapshotVertexCount, t.colId + i * snapshotVertexCount, t.value)
        ei += 1
      }
      
      // add edges between current and the previous snapshots
      // edge direction: from previous to current
      // edge weights: weightless
      for(j:Int <- 0 until snapshotVertexCount.toInt)
      {
        edges(ei) = Edge(j + (i - 1) * snapshotVertexCount, j + i * snapshotVertexCount, 0.0)
        ei += 1
      }
    }
    
    // Generate graph
    val gv:RDD[(VertexId, (VertexId, Double))] = sc.parallelize(vertices)
    val ge:RDD[Edge[Double]] = sc.parallelize(edges)
    val graph = Graph(gv, ge)
    
    // Generate snapshot range lookup table
    val snapshotRange:Array[(Long, Long, Double, Double)] = new Array[(Long, Long, Double, Double)](3)
    for(i:Int <- 0 until snapshotRange.length)
    {
      snapshotRange(i) = (i * snapshotVertexCount, (i + 1) * snapshotVertexCount, i * duration, duration)
    }
    
    return (graph, snapshotRange)
  }
  */
}