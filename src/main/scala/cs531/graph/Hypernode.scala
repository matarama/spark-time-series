package cs531.graph

class Hypernode(
    override val pProperty:Property, override val pId:Int) 
    extends Vertex(pId, pProperty)
{ 
  override def toString() : String = return id + "-" + property.toString()
}