package cs531.graph

import cs531.sparse.COO
import scala.collection.mutable.Map
import cs531.sparse.Triplet

class SparseHypergraph(
    val pHypernodeCount:Int = 0, 
    val pHyperedgeCount:Int = 0, 
    val pHypernodeMap:Map[Int, Hypernode] = Map(), 
    val pHyperedgeMap:Map[Int, Hyperedge] = Map(), 
    val pAdjList:Array[List[Hyperedge]] = null)
{
  var hypernodeCount:Int = pHypernodeCount
  var hyperedgeCount:Int = pHyperedgeCount
  
  var hypernodeMap:Map[Int, Hypernode] = pHypernodeMap
  var hyperedgeMap:Map[Int, Hyperedge] = pHyperedgeMap
  
  var adjList:Array[List[Hyperedge]] = pAdjList
  
  def extractFromCoo(coo:COO): SparseHypergraph = 
  {
    hypernodeCount = coo.rowCount
    hyperedgeCount = coo.nnz
    // adjacency list representation
    adjList = new Array[List[Hyperedge]](hypernodeCount)
    
    var i = 0
    for(i <- 0 until hypernodeCount)
      adjList(i) = Nil
    
    // add hyperedges (columns)
    for(i <- 0 until coo.nnz)
    {
      var tr:Triplet = coo.triplets(i)
      var heId = tr.colId
      var srcId = tr.rowId
      var dstId = tr.colId
      
      var srcHn:Hypernode = getCreateHypernode(srcId)
      var dstHn:Hypernode = getCreateHypernode(dstId)
      var he:Hyperedge = getCreateHyperedge(dstId)
      
      he.srcIds = (srcHn, tr.value) :: he.srcIds
      // this is for matrix 
      if(he.dstIds.isEmpty)
        he.dstIds = (dstHn, 0.0) :: he.dstIds
      
      // adjacency list representation
      adjList(srcId) = he :: adjList(srcId)
    }
    
    return this
  }
  
  private def getCreateHypernode(id:Int): Hypernode = 
  {
    var hn:Option[Hypernode] = hypernodeMap.get(id)
    if(hn.isEmpty)
    {
      var newHn:Hypernode = new Hypernode(new DefaultProperty, id)
      hypernodeMap.put(id, newHn)
      
      return newHn
    }
    else
    {
      return hn.get
    }
  }
  
  private def getCreateHyperedge(id:Int): Hyperedge = 
  {
    var he:Option[Hyperedge] = hyperedgeMap.get(id)
    if(he.isEmpty)
    {
      var newHe:Hyperedge = new Hyperedge(pId = id)
      hyperedgeMap.put(id, newHe)
      
      return newHe
    }
    else
    {
      return he.get
    }
  }
  
  override def toString(): String = 
  {
    var sb:StringBuffer = new StringBuffer()
    
    var i = 0
    for(i <- 0 until adjList.length)
    {
      sb.append(i + " => " + adjList(i) + "\n")
    }
    
    return sb.toString()
  }
}