package cs531.graph

trait Property 
{
  def compareTo(other:Property): Int
  override def toString(): String
}