package cs531.graph

class Satellite(
    val predecessor:Int = -1, 
    val distance:Double = Double.PositiveInfinity) 
{
  var p:Int = predecessor
  var d:Double = distance
  
  override def toString(): String = return "p:" + p + " d=" + d
}