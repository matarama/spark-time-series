package cs531.graph

class CustomSnapshot(
    val pGraph:SparseGraph = null, 
    val pClock:Double = 0.0, 
    val pDuration:Double = 0.0) extends Snapshot
{
  var graph:SparseGraph = pGraph
  var clock:Double = pClock
  var duration:Double = pDuration
  
  override def toString(): String = 
    return "Clock: " + clock + " Duration: " + duration + "\n" + graph
}