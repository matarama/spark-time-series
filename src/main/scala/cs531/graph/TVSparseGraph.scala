package cs531.graph

import cs531.sparse.COO

class TVSparseGraph 
{
  var vertexCount:Int = 0
  var snapshots:Array[CustomSnapshot] = null
  
  def readSnapshots(mmfPaths:String*): TVSparseGraph =
  {
    snapshots = new Array[CustomSnapshot](mmfPaths.length)
    
    var clock:Double = 0.0
    var duration:Double = 5.0
    
    var i = 0
    for(i <- 0 until mmfPaths.length)
    {
      var coo:COO = new COO().parseMMF(mmfPaths(i))
      snapshots(i) = new CustomSnapshot(new SparseGraph().extractFromCoo(coo), clock, duration)
      
      if(snapshots(i).graph.vertexCount > vertexCount)
        vertexCount = snapshots(i).graph.vertexCount
        
      clock += duration
    }
    
    return this
  }
  
  def findSnapshotIndex(time:Double): Int = 
  {
    var p = 0
    var r = snapshots.length - 1
    var index = -1
    
    // binary search snapshots
    while(r >= p && index < 0)
    {
      var q = (r - p) / 2
      
      var comparison:Int = snapshots(q).compareTime(time)
      if(comparison < 0)
        p = q + 1
      else if(comparison > 0)
        r = q - 1
      else
        index = q
    }
    
    return index
  }
  
  def getSnapshot(index:Int): CustomSnapshot = return snapshots(index)
  
  override def toString(): String = 
  {
    var sb:StringBuffer = new StringBuffer()
    var i = 0
    
    for(i <- 0 until snapshots.length)
      sb.append(i + ". " + snapshots(i)) 
    
    return sb.toString()
  }
}