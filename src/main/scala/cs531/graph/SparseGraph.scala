package cs531.graph

import cs531.sparse.COO
import cs531.sparse.Triplet
import scala.collection.mutable.Map


class SparseGraph(
    val pVertexCount:Int = 0, val pEdgeCount:Int = 0)
{
  var vertexCount:Int = pVertexCount
  var edgeCount:Int = pEdgeCount
  
  var vertexMap:Map[String, Vertex] = Map()
  var edgeMap:Map[String, CustomEdge] = Map()
  
  var adjList:Array[List[(Int, Double)]] = null
  var vertices:Array[Vertex] = null
  var edges:Array[CustomEdge] = null
  
  def extractFromCoo(coo:COO): SparseGraph = 
  {
    vertexCount = coo.rowCount
    edgeCount = coo.nnz
    // adjacency list representation
    adjList = new Array[List[(Int, Double)]](vertexCount)
    vertices = new Array[Vertex](vertexCount)
    edges = new Array[CustomEdge](edgeCount)
    
    // add vertices
    var i = 0
    for(i <- 0 until vertexCount)
    {
      var v:Vertex = new Vertex(i, new DefaultProperty(i + ""));
      vertexMap.put(v.property.toString(), v);
      
      vertices(i) = v
      adjList(i) = Nil
    }

    // add edges (non-zero entries)
    for(i <- 0 until coo.nnz)
    {
      var tr:Triplet = coo.triplets(i)
      var e:CustomEdge = new CustomEdge(new DefaultProperty, tr.rowId, tr.colId, tr.value)
      
      edgeMap.put(e.getKey(), e)
      edges(i) = e
      adjList(e.srcId) = (e.dstId, e.weight) :: adjList(e.srcId)
    }
    
    return this
  }
  
  def initializeSingleSource(s:Vertex): Unit =
  {
    vertices.foreach 
    { 
      v => 
        v.satellite.d = Double.PositiveInfinity 
        v.satellite.p = -1
    }
    
    s.satellite.d = 0
  }
  
  def printState(): Unit = 
  {
    println("Graph State")
    vertexMap.foreach(t => println(t._2))
  }
  
  override def toString(): String = 
  {
    var sb:StringBuffer = new StringBuffer()
    
    var i = 0
    for(i <- 0 until adjList.length)
    {
      sb.append(i + " =>")
      adjList(i).foreach { pair => sb.append(" " + pair._1 + ":" + pair._2) }
      sb.append("\n")
    }
    
    return sb.toString()
  }
  
}