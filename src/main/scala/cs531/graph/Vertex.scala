

package cs531.graph

class Vertex(
    val pId:Int = -1, 
    val pProperty:Property = new DefaultProperty, 
    val pSatellite:Satellite = new Satellite)
{
  var id:Int = pId
  var property:Property = pProperty
  var satellite:Satellite = pSatellite
  
  override def toString(): String = return id + " " + satellite.toString()
}

object VertexOrdering extends Ordering[Vertex]
{
  override def compare(a:Vertex, b:Vertex) = (b.satellite.d.compareTo(a.satellite.d))
}