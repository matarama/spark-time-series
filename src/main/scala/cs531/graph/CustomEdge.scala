

package cs531.graph

class CustomEdge(
    val pProperty:Property = new DefaultProperty, 
    val pSrcId:Int = -1, val pDstId:Int = -1, 
    val pWeight:Double = 0)
{
  var property:Property = pProperty
  var srcId:Int = pSrcId
  var dstId:Int = pDstId
  var weight:Double = pWeight
  
  def getKey(): String = return srcId + "-" + dstId
  
  override def toString(): String = 
    return property.toString() + " (" + weight + ")" + srcId + " => " + dstId
}