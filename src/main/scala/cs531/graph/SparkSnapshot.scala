package cs531.graph

import cs531.sparse.COO
import cs531.sparse.COO
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.graphx._
import org.apache.spark.broadcast.Broadcast

class SparkSnapshot( 
    val pClock:Double = 0.0, 
    val pDuration:Double = 0.0) extends Snapshot 
{
  var clock:Double = pClock
  var duration:Double = pDuration
  
  // Vertex Property = (predecessorId:VertexId, distance:Double, duration:Double, locked:Boolean)
  // Edge Property = (edge-weight:Double)
  var graph:Graph[(VertexId, Double, Double, Boolean), Double] = null
  
  def extractFromCOO(coo:COO, sc:SparkContext): SparkSnapshot = 
  {    
    val vertexCount = coo.rowCount
    val edgeCount = coo.nnz
    
    // Vertex Property = (predecessorId:VertexId, distance:Double, duration:Double, locked:Boolean)
    var vertices:Array[(VertexId, (VertexId, Double, Double, Boolean))] = new Array[(VertexId, (VertexId, Double, Double, Boolean))](vertexCount)
    for(i:Int <- 0 until vertices.length)
      vertices(i) = (i.toLong, (-1, Double.PositiveInfinity, duration, false))
      
    // (src-vertex-id, dst-vertex-id, weight)
    var edges:Array[Edge[Double]] = new Array[Edge[Double]](edgeCount)
    for(i:Int <- 0 until edges.length)
    {
      edges(i) = Edge(
          coo.triplets(i).rowId.toLong, 
          coo.triplets(i).colId.toLong, 
          coo.triplets(i).value)
    }
    
    // generate spark graph
    val gv:RDD[(VertexId, (VertexId, Double, Double, Boolean))] = sc.parallelize(vertices)
    val ge:RDD[Edge[Double]] = sc.parallelize(edges)    
    graph = Graph(gv, ge)
    
    return this
  }
  
  override def toString(): String = 
    return "Clock: " + clock + " Duration: " + duration + "\n" + graph
}