package cs531.graph


class Hyperedge(
    override val pProperty:Property = new DefaultProperty, 
    override val pId:Int, 
    val pSrcIds:List[(Hypernode, Double)] = List(), 
    val pDstIds:List[(Hypernode, Double)] = List()) 
    extends Vertex(pId:Int, pProperty:Property)
{
  var srcIds:List[(Hypernode, Double)] = pSrcIds
  var dstIds:List[(Hypernode, Double)] = pDstIds
  
  override def toString(): String = 
  {
    var sb:StringBuffer = new StringBuffer()
    var i = 0
    
    sb.append("e-" + id + "(" + property.toString() + "): ")
    sb.append("{")
    srcIds.foreach { x => sb.append(x._1.pId + "(" + x._2 + ") ") }
    sb.append("=>")
    dstIds.foreach { x => sb.append(" " + x._1.pId + "(" + x._2 + ")") }
    sb.append("}")
    
    return sb.toString()
  }
}