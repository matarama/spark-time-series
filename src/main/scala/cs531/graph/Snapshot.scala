package cs531.graph

trait Snapshot 
{
  var clock:Double
  var duration:Double
  
  
  /**
   * Returns: <br>
   * <ul>
   *  <li>0 if time overlaps with the current snapshot</li>
   *  <li>1 if current snapshot is ahead of time</li>
   *  <li>-1 if time is ahead of current snapshot</li>
   * </ul>
   */
  def compareTime(time:Double): Int =
  {
    if(clock + duration <= time)
      return -1
    else if(time < clock)
      return 1
    else // if((clock <= time) && (clock + duration > time))
      return 0
    
  }
  
  def doesOverlap(time:Double): Boolean = return compareTime(time) == 0
  
  override def toString(): String
}