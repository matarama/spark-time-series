package cs531.graph

class DefaultProperty(val pData:String = "") extends Property 
{
  var data:String = pData
  
  override def compareTo(other:Property): Int = 
    return data.compareTo(other.asInstanceOf[DefaultProperty].data)
    
  override def toString(): String = return data
}